# HPC Assignment 2 - Parallel image convolutions

## Dependencies 
Compiling C code  
* `sudo apt install make`
* `sudo apt install gcc`

CUDA
* NVidia Graphics Card 
* [install CUDA](https://www.pugetsystems.com/labs/hpc/How-to-install-CUDA-9-2-on-Ubuntu-18-04-1184/) 


## run.sh 
To run all of the implmentations, type `bash run.sh` in the terminal 

All images are output to the `/data` folder.

Timing results for each implmentation can be found in their respective folders in .csv format e.g. `Problem 2/src/results.csv` and similarly output from NV Profiler can be found in a similarly located `prof_results.csv`

Please note that using NV Profiler can alter the GPU timing quite drastically. 

If one wishes to run an NV Profiler the terminal command is 
`nvprof --print-gpu-trace --metrics flops_sp --metrics flops_sp_fma --metrics gld_transactions --log-file nvprof_results.csv --csv ./const_img_conv`

With the end program subitituted accordingly. 


Some result images are avaiable, however to keep the file size small larger result images have been omitted. To view these output images, one should run `run.sh` 

## Report 
see Report.pdf 

# Problem Specification 
see assignment2.pdf 
<br>
<br>
![](https://i.gyazo.com/1ca41b506ea04f7e271684347992967e.png)
![](https://i.gyazo.com/0ecb97a6015f8222654717d9b8f6db10.png)

