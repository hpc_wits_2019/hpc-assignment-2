//serial_img_conv.h

#ifndef SERIAL_IMG_CONV_H // from example - why are these important ??
#define SERIAL_IMG_CONV_H

__host__ void importImage_Data(const char* fileName,float* h_Data, uint* width, uint* height);

__host__ void export_Image_Data (const char* fileName, float* h_OutputData, uint width, uint height);

__host__ void convolve (float* in_Data, float* out_Data, uint data_Width, uint data_Height, const float* mask, uint mask_Size);

#endif
